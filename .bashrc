# https://gitlab.com/bentinata/dotfiles

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Load from .d.
if [[ -d ~/.d ]]; then
  for rc in ~/.d/*.bash; do
    source "$rc"
  done
  unset rc;
fi
