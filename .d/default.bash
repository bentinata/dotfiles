# Enable bash feature.
# Why one would develop feature if it's unused?
option=(
# Don't overwrite history, append it.
histappend
# Save multiline command into one.
cmdhist
# RegEx globbing
extglob
# No hidden
dotglob
# Expand "**".
globstar
# Check dimension and update LINES and COLS if necessary.
checkwinsize

autocd
cdspell
)
# No need for loop!
shopt -s ${option[@]} 2> /dev/null

PROMPT_COMMAND='history -a'
PS1='\[\033[01;33m\]❄️ \h\[\033[00m\]:\[\033[01;90m\]\w\[\033[00m\]\$ '

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
  test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"

  # Colored diff.
  alias diff='diff -u --color=auto'
  # X: extension, h: human, x: lines
  alias ls='ls -Xhx --color=auto --group-directories-first'
  alias grep='grep --color=auto'
fi

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi
