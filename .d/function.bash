# vi: syn=sh
cd() {
  builtin cd "$@"

  local paths=(
  "node_modules/.bin"
  )

  for p in $paths; do
    case "$PATH" in
      *"$p"*)
        if [[ ! -d "$p" ]]; then
          PATH=${PATH//":$p"/}
        fi
      ;;
      *) if [[ -d "$p" ]]; then
          PATH="$PATH:$p"
        fi
      ;;
    esac
  done

  unset paths
}

=() {
  echo $(("$@"))
}
