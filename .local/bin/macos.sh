#!/usr/bin/env bash

osascript -e 'tell application "System Preferences" to quit'

remap_on() {
  hidutil property --set '{
    "UserKeyMapping":[{
      "HIDKeyboardModifierMappingSrc":0x700000039,
      "HIDKeyboardModifierMappingDst":0x700000029
    }]
  }'
}

remap_off() {
  hidutil property --set '{"UserKeyMapping":[]}'
}

case "$1" in
  "0")
    remap_off
    ;;
  "1")
    remap_on
    ;;
  *)
    remap_on
    ;;
esac

defaults write com.microsoft.VSCode ApplePressAndHoldEnabled -bool false
