call plug#begin('~/.vim/plugged')
  Plug 'sheerun/vim-polyglot'
  Plug 'vim-airline/vim-airline'

  if !empty(glob("~/.vim/local-plug.vim"))
    source ~/.vim/local-plug.vim
  endif
call plug#end()

" neovim defaults
syntax enable
filetype plugin indent on
set autoindent
set autoread
set backspace=indent,eol,start
set complete-=i
set display+=lastline
set encoding=utf-8
set formatoptions+=j
set history=10000
set hlsearch
set incsearch
set laststatus=2

" overriden
" set listchars=tab:>\ ,trail:-,nbsp:+
set mouse=a
set nocompatible
set nrformats-=octal
set sessionoptions-=options
set smarttab
set tabpagemax=50
set tags-=./tags tags^=./tags;
set ttyfast
set viminfo^=!
set wildmenu

" theme
set guicursor=
set t_Co=16
set background=dark
colorscheme noctu
" let base16colorspace=256
let g:airline_theme='minimalism'
" silent! colorscheme base16-monokai

" indent width & force tab to spaces
set tabstop=2
set shiftwidth=2
set shiftround
set softtabstop=2
set expandtab
set smartindent

" encodings
set fileencoding=utf-8

" wrap past multiline on <h> and <l>
set ww+=h,l
set linebreak
set breakat=\ ^I.{,80}

" no need to write for buffer switching
set hidden

" reduce that braces yo
set foldmethod=syntax
set foldlevel=99
set foldlevelstart=99

" need for speed
set lazyredraw

" combined number and relativenumber
set nu
set rnu

" show special char
set conceallevel=2
set listchars=tab:→\ ,
" set listchars=space:·

" CLIPBOARD
set clipboard+=unnamedplus

" all tmp in same place
set swapfile
set dir=/tmp
set backupdir=/tmp

" wildmenu options
set wildmode=full
set wildignore+=*/tmp/*,*.swp,*/.git/*

" open split in below and right instead of top and left
set splitbelow
set splitright

" wesmart search
set ic
set scs

" that 80 col marker
highlight ColorColumn ctermbg=1
call matchadd('ColorColumn', '\%80v')
call matchadd('ColorColumn', '\%100v')

if executable('rg')
  set grepprg=rg\ --vimgrep
endif

let g:airline_right_sep=''
let g:airline_left_sep=''
let g:airline#extensions#tabline#enabled = 1
let g:airline_mode_map = {
\ '__' : '-',
\ 'n'  : 'N',
\ 'i'  : 'I',
\ 'R'  : 'R',
\ 'c'  : 'C',
\ 'v'  : 'V',
\ 'V'  : 'V',
\ '' : 'V',
\ 's'  : 'S',
\ 'S'  : 'S',
\ '' : 'S',
\ }

let g:javascript_plugin_jsdoc = 1
let g:javascript_conceal_function = "ƒ"

" remove trailingwhitespace
function! <SID>TrimWhitespaces()
  if &ft =~ 'markdown'
    return
  endif

  let cursor = getpos('.')
  %s/\s\+$//e
  call setpos('.', cursor)
endfun

augroup trim_whitespace
  au!
  au BufWritePre * :call <SID>TrimWhitespaces()
augroup END

" don't open fold below unmatched fold
augroup manual_fold
  au!
  au InsertEnter * setlocal foldmethod=manual
  au InsertLeave,WinLeave * setlocal foldmethod=syntax
augroup END

" some highlight
augroup hilite
  au!
  au BufRead,BufNewFile *.profile     set filetype=sh
  au BufRead,BufNewFile /etc/nginx/*  set filetype=nginx
  au BufRead,BufNewFile /etc/php*/*   set filetype=dosini
augroup END

if !empty(glob("~/.vim/local.vim"))
	source ~/.vim/local.vim
endif

nnoremap / :let @/=""<CR> /
