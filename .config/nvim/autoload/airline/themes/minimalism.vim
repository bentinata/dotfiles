let g:airline#themes#minimalism#palette = {}

let s:N1 = [ '#00dfff', '#000000', 4, 0 ]
let s:N2 = [ '#ff8822', '#000000', 3, 0 ]
let s:N3 = [ '#aaaaaa', '#000000', 7, 0 ]
let s:NM = [ '#dd2244', '#000000', 9, 0 ]

let g:airline#themes#minimalism#palette.normal = airline#themes#generate_color_map(s:N1, s:N2, s:N3)
let g:airline#themes#minimalism#palette.normal_modified = { 'airline_c': s:NM }

let s:I1 = [ '#aaee22', '#000000', 2, 0 ]
let s:I2 = [ '#ff8822', '#000000', 3, 0 ]
let s:I3 = [ '#aaaaaa', '#000000', 7, 0 ]
let s:IP = [ '#aaee22', '#ff8822', 2, 3, '' ]
let g:airline#themes#minimalism#palette.insert = airline#themes#generate_color_map(s:I1, s:I2, s:I3)
let g:airline#themes#minimalism#palette.insert_modified = copy(g:airline#themes#minimalism#palette.normal_modified)
let g:airline#themes#minimalism#palette.insert_paste = { 'airline_a': s:IP }

let s:R1 = [ '#aaee22', '#dd2244', 2, 9, '' ]
let g:airline#themes#minimalism#palette.replace = { 'airline_a': s:R1 }
let g:airline#themes#minimalism#palette.replace_modified = copy(g:airline#themes#minimalism#palette.normal_modified)

let s:V1 = [ '#ffcc66', '#000000', 11, 0 ]
let s:V2 = [ '#ff8822', '#000000', 3, 0 ]
let s:V3 = [ '#aaaaaa', '#000000', 7, 0 ]
let g:airline#themes#minimalism#palette.visual = airline#themes#generate_color_map(s:V1, s:V2, s:V3)
let g:airline#themes#minimalism#palette.visual_modified = copy(g:airline#themes#minimalism#palette.normal_modified)

let s:IA  = [ '#aaaaaa', '#000000', 7, 0, '' ]
let s:IA2 = [ '#aaaaaa', '#222222', 7, 8, '' ]
let g:airline#themes#minimalism#palette.inactive = airline#themes#generate_color_map(s:IA, s:IA2, s:IA2)
let g:airline#themes#minimalism#palette.inactive_modified = copy(g:airline#themes#minimalism#palette.normal_modified)
