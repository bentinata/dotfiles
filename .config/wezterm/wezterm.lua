local wezterm = require "wezterm"
local tango = wezterm.color.get_builtin_schemes()["Tango (terminal.sexy)"]
tango.foreground = "fffefe"

wezterm.on("toggle-opacity", function(window, pane)
  local overrides = window:get_config_overrides() or {}

  if not overrides.window_background_opacity then
    overrides.window_background_opacity = 0
    overrides.text_background_opacity = 0
  else
    overrides.window_background_opacity = nil
    overrides.text_background_opacity = nil
  end

  window:set_config_overrides(overrides)
end)


return {
  default_prog = { "/opt/homebrew/bin/fish" },

  hide_tab_bar_if_only_one_tab = true,
  initial_rows = 24 * 2,
  initial_cols = 80 * 2,

  font = wezterm.font("Monaco"),
  font_size = 10,
  color_schemes = {
    ["tango"] = tango,
  },
  color_scheme = "tango",

  keys = {
    {
      key = "T",
      mods = "SUPER|SHIFT",
      action = wezterm.action.Multiple {
        wezterm.action.SplitHorizontal { domain = "CurrentPaneDomain" },
      },
    },
    {
      key = "H",
      mods = "SUPER|SHIFT",
      action = wezterm.action.EmitEvent("toggle-opacity"),
    },
  },

  hyperlink_rules = {
    -- Linkify things that look like URLs and the host has a TLD name.
    -- Compiled-in default. Used if you don't specify any hyperlink_rules.
    {
      regex = '\\b\\w+://[\\w.-]+\\.[a-z]{2,15}\\S*\\b',
      format = '$0',
    },
    -- Linkify things that look like URLs with numeric addresses as hosts.
    -- E.g. http://127.0.0.1:8000 for a local development server,
    -- or http://192.168.1.1 for the web interface of many routers.
    {
      --regex = [[\b\w+://(?:[\d]{1,3}\.){3}[\d]{1,3}\S*\b]],
      regex = [[\b(https?://)?[\w\d\.]+\S*\b]],
      format = '$0',
    },
  },
}
